import java.util.*;

public class MangoTree {
    public static void main(String[] args)
    {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter number of rows");
        int row=sc.nextInt();
        System.out.println("Enter number of columns");
        int col=sc.nextInt();
        System.out.println("Enter tree number");
        int tree_number=sc.nextInt();
        ArrayList<Integer> arrli = new ArrayList<>(row*col);
        int leftcol=1,rightcol=col;
        //here I am going to add first column elements to the array list based on the rows and columns they given except starting element
        for (int i = 1; i <row; i++) {
            leftcol = leftcol + col;
            arrli.add(leftcol);
        }
        //here I am going to add last column elements to the array list based on the rows and columns they given except starting element of right column
        for (int i = 1; i <row; i++) {
            rightcol = rightcol + col;
            arrli.add(rightcol);
        }
        //here I am going to add first row elements to the array list
        for (int i = 1; i <=col; i++)
            arrli.add(i);

        System.out.println(arrli);
        int flag=0;
        //here I will check whether given tree number present in arraylist or not .If present I will yes else print no
        for (int i = 0; i < arrli.size(); i++)
        {
            if((arrli.get(i)==tree_number))
            {
                System.out.print("yes");
                flag=1;
                break;
            }
        }
        if(flag==0)
            System.out.print("no");



    }
}








